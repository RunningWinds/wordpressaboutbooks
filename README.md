Blog on wordpress dedicated to books. Implemented the ability to register and write comments under the posts.
Used plugins:
- Classic Editor,
- Contact Form 7,
- Custom Login Page Customizer,
- elementor,
- Kama Click Counter,
I also wrote a couple of my own.
View it running here: http://narchie.beget.tech/