<?php
/*
  Plugin Name: Подпись для статей
  Description: Подписывает статьи и показывает количество просмотров
  Version: 1.0
  Author: Арчибальд
  Author URI: https://gitlab.com/RunningWinds
*/

add_filter('the_content', 'sign_content');

function sign_content($content){
	if( !is_single() ) return $content;
	$sign = '<div class="alignright"><em>Signed by Holy Carol....</em></br><em>Views: '.get_view_count().'</em></div>';
	return $content . $sign;
}

add_action("wp_head", "add_view");
/**
 * @global object $post Объект поста
 * @return integer $new_views Количество просмотров поста
 */
function add_view(){
  if (is_single()) {
    global $post;
    $current_views = get_post_meta($post->ID, "views", true);
    if (!isset($current_views) OR empty($current_views) OR !is_numeric($current_views)){
      $current_views = 0;
    }
    $new_views = $current_views + 1;
    update_post_meta($post->ID, "views", $new_views);
    return $new_views;
  }
}
/**
 * 
 */
function get_view_count(){
  global $post;
  $current_views = get_post_meta($post->ID, "views", true);
  if(!isset($current_views) OR empty($current_views) OR !is_numeric($current_views) ) {
    $current_views = 0;
  }

  return $current_views;
}
?>