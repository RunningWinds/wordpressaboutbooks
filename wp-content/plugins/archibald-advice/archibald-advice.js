async function getAdvice() {
  let response = await fetch("https://api.adviceslip.com/advice");
  if (response.ok) {
    let json = await response.json();
    document.cookie = "advice=" + json.slip.advice;
  } else {
    console.log("Ошибка HTTP: " + response.status);
  }
}
getAdvice();