<?php
/*
Plugin Name: Советы
Description: Выдает рандомный совет с удаленного АПИ в футере
Version: 1.0
Author: Арчибальд
Author URI: https://gitlab.com/RunningWinds
 */


function get_advice_from_api() { 
    wp_enqueue_script( 'archibald-advice', plugin_dir_url( __FILE__ ) . '/archibald-advice.js' );
}


add_action( 'wp_footer', 'get_advice_from_api' );

function get_advice_cookie(){
    if (isset($_COOKIE['advice'])){
        echo "<b><span>".$_COOKIE['advice']."</span></b>";
    }
    else {
        echo "<b><span>Seize the summer!</span></b>";
    }
}
