<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'counters_wp' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[Iyd**F{#mo2}u]LcU>bw1^^~[=$*rMQ(92~A&z9k8EMr+]47h -*k;bzOO|rmU)' );
define( 'SECURE_AUTH_KEY',  'dl%4_btmI73b)jh5XS!P>F2$m#EW >WJP=A1<!q[E3^T5q<1?tVL.m~f#Bz+2_ZR' );
define( 'LOGGED_IN_KEY',    'ohYTDg<sZ|=PEqDIOJRUSfPD?;LsN2q+Tc njF;JM@ *YlAUqGNr%sERw(m 0J3x' );
define( 'NONCE_KEY',        'Nh+[rF8?>%)1:$%v%`5[qSkre4SQTt^$AV{ U-.El3|*fg*zqY];Aj;+AF(}Q/o1' );
define( 'AUTH_SALT',        ']|$#R<}#0^5Ea|+?&=^|]Q[g2%cV|c|x0^/J#t~{91pi?crwRP%4,R_Ul$.Fm wC' );
define( 'SECURE_AUTH_SALT', '2]l?mG/{iG.oQ9msKS7{o1U~?sb5{{l:3t0{.tqH@F$hX%_r5HNy3sz&]cVLk<XQ' );
define( 'LOGGED_IN_SALT',   '@sr#( (FsE~,U5ADRP]TzfeNkId%kpv8v;4/Hi#}UfJ{@VgV;iM<>hwXy-&AH~qy' );
define( 'NONCE_SALT',       'b$q}(Yw)7y$}},F30EHgi{w-( blO3#dO4+T>reU}|^qoez]-*Ppq SxE^@Pc#;e' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
